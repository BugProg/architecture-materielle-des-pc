$(document).ready(function(){

  $("#TransistorsContent").hover(function(){
    $('#TransistorsAudio').get(0).play();
  }),

  $("#HardDriveContent").hover(function(){
    $('#HardDriveAudio').get(0).play();
  }),

  $("#ArpanetContent").hover(function(){
    $('#ArpanetAudio').get(0).play();
  });

  $("#ProcesseurContent").hover(function(){
    $('#ProcesseurAudio').get(0).play();
  });

  $("#GPSContent").hover(function(){
    $('#GPSAudio').get(0).play();
  });

  $("#PhotosContent").hover(function(){
    $('#PhotosAudio').get(0).play();
  });

  $("#TableurContent").hover(function(){
    $('#TableurAudio').get(0).play();
  });

  $("#CocaContent").hover(function(){
    $('#CocaAudio').get(0).play();
  });

  $("#InternetContent").hover(function(){
    $('#InternetAudio').get(0).play();
  });

  $("#InternetObjetsContent").hover(function(){
    $('#InternetObjetsAudio').get(0).play();
  });

  $("#IphonesContent").hover(function(){
    $('#IphonesAudio').get(0).play();
  });

});
